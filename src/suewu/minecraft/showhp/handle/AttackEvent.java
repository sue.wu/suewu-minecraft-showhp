package suewu.minecraft.showhp.handle;

import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import suewu.minecraft.showhp.Main;

public class AttackEvent implements Listener {

    private Main plugin;

    public AttackEvent(Main plugin){
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onAttackEvent(EntityDamageByEntityEvent entityDamageByEntityEvent){
        Bukkit.broadcastMessage("-- attack event --");
        showHPMsg(entityDamageByEntityEvent);
    }

    private void showHPMsg(EntityDamageByEntityEvent entityDamageByEntityEvent){
//        Bukkit.broadcastMessage("broadcastMessage EntityDamageByEntityEvent=" + entityDamageByEntityEvent.toString());
        Entity damager = entityDamageByEntityEvent.getDamager();
        if(damager instanceof Player)
        {
            Player player = (Player) damager;
            Entity entity = entityDamageByEntityEvent.getEntity();
            player.sendMessage(player.getDisplayName() + " 攻擊 " + entity.getName());

            LivingEntity livingEntity = (LivingEntity)entity;
            player.sendMessage("目標 "+livingEntity.getName() + " 的剩餘血量=" + String.valueOf(livingEntity.getHealth()));
            player.sendMessage("目標 "+livingEntity.getName() + " 的剩餘血量=" + String.valueOf(livingEntity.getHealth()));
//            Bukkit.broadcastMessage("broadcastMessage EntityDamageEvent's livingEntity=" + livingEntity.toString());
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage("player test mod! 'show hp'");
    }
}
